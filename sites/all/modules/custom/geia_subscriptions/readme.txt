*/15 * * * * wget -O - -q -t 1 http://geiafit.dev.cpcs.ws/cron.php?cron_key=buRDQ3mx08mDqCog2TtQr6lcmaX3BmyGv5_vW3ab-8E
// ++
Добавление новых полей через модуль profile2:
--------------------------------------------
в частности используется для добавления поля amount для пациента
http://geiafit.dev.cpcs.ws/admin/structure/profiles/manage/main/fields

Алгорит работы с API:
--------------------
1) Все запросы должны быть авторизированы в системе Drupal:
   Цитата из: (https://github.com/gplsek/geia_api)

   Login POST: /user/login formdata: username and password
   returns user oject and : session_id , session_name and token
   All subsequent requests need to be made with Cokkie and Token in the header Cooke = session_name+'='+session_id Token = token
   Example: "sessid": "lMaG__9q3BRep76TfR1jDYWiGHPtZEmQxygqPZ7AOGU", "session_name": "SESS5dc4399c78481c1276416448d9e45919", "token": "tY35Ns_7TU6ktjz6pmF4R7oKwxGS3V6Hv9Ig7cG-r6M",
   get user profile GET: profile/1
   ```sh
   header: Cookie: SESS5dc4399c78481c1276416448d9e45919=lMaG__9q3BRep76TfR1jDYWiGHPtZEmQxygqPZ7AOGU
           Token: tY35Ns_7TU6ktjz6pmF4R7oKwxGS3V6Hv9Ig7cG-r6M
   ```
   Logout POST: user/logout


2) При загрузке приложения делаем вызов API:
   http://geiafit.dev.cpcs.ws/api/bt/check

   Данный метод возвращает текущее состояние подписки.
   Пример результата и основные действия по интерпритированию результата:
   // Пример ответа, когда все ок и можна пускать пользователя дальше в приложение
   {
     allowed: true,
     message: 'Your subscription plan is Patient1 / 200.00 USD / per 1 month',
   }

   // Пример ответа, когда нужно запросить платежную информацию у клиента
   // через BrainTree API for IOS (https://developers.braintreepayments.com/guides/client-sdk/setup/ios/v4)
   // для запроса используется token
   {
      allowed: false,
      message: 'Please provide your payment information',
      token  : 'eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJlNDUyYTZmMDVlMzMzN2U1MGE4YWUxZjAxNTJhZTdjNWNmMWE1Y2M2MjliOWY2MGJjNTA0ZTNmMjEwMTBmOTczfGNyZWF0ZWRfYXQ9MjAxNi0wOS0zMFQxNDo0NDo1My4wOTk1NDcwMTIrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTNwOXRqY2J5ZnczcXZwcnFcdTAwMjZwdWJsaWNfa2V5PWd5d3o5dDMzbTc5OG16NTUiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvM3A5dGpjYnlmdzNxdnBycS9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzNwOXRqY2J5ZnczcXZwcnEvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzNwOXRqY2J5ZnczcXZwcnEifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiR2xvYmFsIFRlY2giLCJjbGllbnRJZCI6bnVsbCwicHJpdmFjeVVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS9wcCIsInVzZXJBZ3JlZW1lbnRVcmwiOiJodHRwOi8vZXhhbXBsZS5jb20vdG9zIiwiYmFzZVVybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXNzZXRzVXJsIjoiaHR0cHM6Ly9jaGVja291dC5wYXlwYWwuY29tIiwiZGlyZWN0QmFzZVVybCI6bnVsbCwiYWxsb3dIdHRwIjp0cnVlLCJlbnZpcm9ubWVudE5vTmV0d29yayI6dHJ1ZSwiZW52aXJvbm1lbnQiOiJvZmZsaW5lIiwidW52ZXR0ZWRNZXJjaGFudCI6ZmFsc2UsImJyYWludHJlZUNsaWVudElkIjoibWFzdGVyY2xpZW50MyIsImJpbGxpbmdBZ3JlZW1lbnRzRW5hYmxlZCI6dHJ1ZSwibWVyY2hhbnRBY2NvdW50SWQiOiJnbG9iYWx0ZWNoIiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sImNvaW5iYXNlRW5hYmxlZCI6ZmFsc2UsIm1lcmNoYW50SWQiOiIzcDl0amNieWZ3M3F2cHJxIiwidmVubW8iOiJvZmYifQ==',
      need_input_payment_info: true
   }

   // Пример ответа с ошибкой
      {
        success: false,
        message: "You don't have permission. Please call to office.",
        error_code: 100
      }



3) После успешного запроса платежной ифнормации у клиента, вызываем данный метод для оформления подписки
   http://geiafit.dev.cpcs.ws/api/bt/subscribe/<nonceFromTheClient>
   nonceFromTheClient     - клиенткский токен который получали при запросе клиентсвой платежной информации из предідущего пункта


