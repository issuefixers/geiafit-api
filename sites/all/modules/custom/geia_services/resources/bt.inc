<?php


function getCurrentUserSubscription () {
    global $user;
    $sql = "select * from `geia_subscriptions` where user_id = ".($GLOBALS['user']->uid)." ORDER BY `create_date` DESC limit 0,1";
    // xlog('subscribe', $sql);
    $r = db_query($sql);
    $subscription = $r->fetchAssoc();
    // xlog('subscribe', $subscription);
    /*if (!empty($subscription)) {
        $user_profile = profile2_load_by_user($user, 'main');
        $amount = field_get_items('profile2', $user_profile, 'field_amount');
        $amount = ((!empty($amount))?$user_profile->field_amount['und'][0]['value']:0);
        //xlog('subscribe', $amount);
        db_update('geia_subscriptions')->fields([
          'amount'               => $amount,
        ])->condition('id', $subscription['id'], '=')->execute();
    }*/
    return $subscription;
}

function getCurrentUserTherapist () {
    $sql = "
      select `users`.*
      from `field_data_field_therapist`
      left join `users` on `users`.`uid` = `field_data_field_therapist`.`field_therapist_uid`
      where `field_data_field_therapist`.`entity_id` = ".($GLOBALS['user']->uid)."
      limit 0,1";
    // xlog('subscribe', $sql);
    $r = db_query($sql);
    $r = $r->fetchAssoc();
    // xlog('subscribe', $subscription);
    return $r;
}

// Try create new subscription
function createNewSubscription () {
   global $user;
  // $user_profile = profile2_load_by_user($user, 'main');
   $therapist = getCurrentUserTherapist();
   if (!empty($therapist)) {
    /*    $amount = field_get_items('profile2', $user_profile, 'field_amount');
        $amount = ((!empty($amount))?$user_profile->field_amount['und'][0]['value']:0);*/
        db_insert('geia_subscriptions')
            ->fields(array(
                'user_id'              => $GLOBALS['user']->uid,
                'trainer_id'           => $therapist['uid'],
                'amount'               => 0,
            ))->execute();
        $subscription = getCurrentUserSubscription();
        return $subscription;
   } else {
     return 'Not found therapist. Please add therapist for patient '.$user->mail;
   }
}

// BrainTree API
function bt_api($action, $param1 = null, $param2 = null, $param3 = null) {
    ob_end_clean();
    global $user;
	//$user = user_load(37);
   // global $base_url;

    $res = [
        "success" => true,
        "logged"  => user_is_logged_in(),
        "action"  => $action,
        "param1"  => $param1,
        "param2"  => $param2,
        "param3"  => $param3,
    ];

    if (!user_is_logged_in()) {
      $res["success"] = false;
      $res["message"] = 'Not authorized';
      return $res;
    }

    $user_profile = profile2_load_by_user($user, 'main');
    $res["client_name"] = $user_profile->field_first_name['und'][0]['value'].' '.$user_profile->field_last_name['und'][0]['value'];
    $res["client_email"] = $user->mail;
    //xlog('user', $user);


    if (!empty($action)) {
      init_bt();

      switch ($action) {
          case 'test': {
             // $user_profile = profile2_load_by_user($user, 'main');
             // xlog('subscribe', $user);
             // xlog('subscribe', $user->mail);
             // xlog('subscribe', $user_profile->field_first_name['und'][0]['value']);
             // xlog('subscribe', $user_profile->field_last_name['und'][0]['value']);
             // getCurrentUserSubscription();

             // xlog('test', $_REQUEST);

             break;
          }

          // Check subscription
          case 'check': {
			  
			  //temp fix
              // $res["allowed"] = true;
 //              $res["message"] = 'You have free enterprise plan. No payments need.';
 //              break;
			  
			  
			  
            $res["allowed"]  = false;
            $subscription = getCurrentUserSubscription();
            if (empty($subscription)) {
              $res["success"]    = false;
              $res["error_code"] = 100;
              $res["message"]    = 'You don\'t have permission. Please call the office.';
              break;
            }

            // 1 - Get date of user subscription creation
            $res["created_date"]      = $subscription['create_date'];
            $res["last_payment_date"] = $subscription['payment_date'];
            $res["next_payment_date"] = null;

            // 2 - Get number of free days
            $res["geia_subscriptions_number_free_days"] = variable_get('geia_subscriptions_number_free_days', 0);

            // 3 - Calc free mode if true continue work app, else continue logic check out
            $freeDate = strtotime("+".variable_get('geia_subscriptions_number_free_days', 0)." day", strtotime($subscription['create_date']));
            if ($freeDate > time() && $res["geia_subscriptions_number_free_days"] > 0) {
               $res["allowed"] = true;
               $res["message"] = 'You have free period to '.date('d M Y', $freeDate).' No payments need.';
               break;
            }

            // 4 - Check Enterprise free plan
            if ($subscription['plan_id'] == -1) {
               $res["allowed"] = true;
               $res["message"] = 'You have free enterprise plan. No payments need.';
               break;
            }

            if (empty($subscription['client_id']) || empty($subscription['client_payment_type'])) {
              $res["allowed"]  = false;
             // $res["customerId"]  = $user->uid;
             // $clientToken  = Braintree_ClientToken::generate();
              $clientToken  = Braintree_ClientToken::generate(array("customerId" => intval($user->uid)));
              $res["token"] = $clientToken;
              $res["need_input_payment_info"] = true;
			  $res["message"]  = 'Your subscription plan is '.$subscription['plan_name'];
              $res["message"]  .= '<br>Please provide your payment information';
              $res["plan"]  =  $subscription['amount'];
            } else {
              $res["allowed"]  = true;
              $res["message"]  = 'Your subscription plan is '.$subscription['plan_name'];
              $res["plan"]  =  $subscription['amount'];
            }
            break;
          }

          // Create subscription
          case 'subscribe': {
              $paymentMethodNonce = $param1;
              if (empty($paymentMethodNonce)) {
                  $res["success"]    = false;
                  $res["error_code"] = 99;
                  $res["message"]    = 'Please provide payment token';
                  break;
              }

              $subscription = getCurrentUserSubscription();
              if (empty($subscription)) {
                  $res["success"]    = false;
                  $res["error_code"] = 100;
                  $res["message"]    = 'You don\'t have permission. Please call the office.';
                  break;
              }

              // Try create new client on BrainTreeAPI
              if (empty($subscription['client_id'])) {
                  $resultCustomer = Braintree_Customer::create([
                      'firstName'          => $user_profile->field_first_name['und'][0]['value'],
                      'lastName'           => $user_profile->field_last_name['und'][0]['value'],
                      'paymentMethodNonce' => $paymentMethodNonce
                  ]);



                  if ($resultCustomer->success) {
                      $paymentMethod = $resultCustomer->customer->paymentMethods[0];


                      $subscription['client_id']           = $resultCustomer->customer->id;
                      $subscription['client_payment_type'] = $resultCustomer->customer->paymentMethods[0]->token;
                      db_update('geia_subscriptions')->fields([
                          'client_id'           => $subscription['client_id'],
                          'client_payment_type' => $subscription['client_payment_type'],
                          'card_type'           => $paymentMethod->cardType,
                          'bin'                 => $paymentMethod->bin,
                          'last_4'              => $paymentMethod->last4,
                          'expiration_date'     => $paymentMethod->expirationDate,
                      ])->condition('id', $subscription['id'], '=')->execute();
                  } else {
                      $res["success"] = false;
                      $res["message"] = 'Error while creating new customer entity: '."\n";
                      $i = 1;
                      foreach($resultCustomer->errors->deepAll() as $error) {
                          $res["errors"][] = $error->code . ": " . $error->message;
                          $res["message"] .= $i.') '.$error->code . ": " . $error->message."\n";
                          $i++;
                      }
                      return $res;
                  }
              } else {
                  // Try create new payment type on BraineTreeAPI
                  if (empty($subscription['client_payment_type'])) {
                      $resultPaymentMethod = Braintree_PaymentMethod::create([
                          'customerId'         => $subscription['client_id'],
                          'paymentMethodNonce' => $paymentMethodNonce
                      ]);

                    //  xlog('payment', $resultPaymentMethod);

                      if ($resultPaymentMethod->success) {
                          $subscription['client_payment_type'] = $resultPaymentMethod->paymentMethod->token;
                          db_update('geia_subscriptions')->fields([
                              'client_payment_type'  => $subscription['client_payment_type'],
                              'card_type'            => $resultPaymentMethod->paymentMethod->cardType,
                              'bin'                  => $resultPaymentMethod->paymentMethod->bin,
                              'last_4'               => $resultPaymentMethod->paymentMethod->last4,
                              'expiration_date'      => $resultPaymentMethod->paymentMethod->expirationDate,
                          ])->condition('id', $subscription['id'], '=')->execute();
                      } else {
                          $res["success"] = false;
                          $res["message"] = 'Error while creating new payment method: '."\n";
                          $i = 1;
                          foreach($resultPaymentMethod->errors->deepAll() as $error) {
                              $res["errors"][] = $error->code . ": " . $error->message;
                              $res["message"] .= $i.') '.$error->code . ": " . $error->message."\n";
                              $i++;
                          }
                          return $res;
                      }


                  }
              }

              // Try create new Braintree subscription
              $result = Braintree_Subscription::create([
                'paymentMethodToken' => $subscription['client_payment_type'],
                'planId'             => $subscription['plan_id']
              ]);
              $res["success"] = true;
              $res["message"] = 'You successfully created subscription for geiafit'."\n";

              break;
          }



          default : {
            $res["success"] = false;
            $res["message"] = 'Empty api action';
            break;
          }
      }
    } else {
      $res["success"] = false;
    }
    ob_end_clean();
   // exit;
    return $res;
}


?>                                                                                                              