<?php
/**
 * @file
 * Contains the resource callbacks for webex.
 */

function geia_services_ptexlib_update_access() {
    return TRUE;
}

function geia_services_ptexlib_delete_access() {
    return TRUE;
}

/**
 * Returns a collection of exercisses.
 *
 * @see geia_services_index_entities()
 */



 function geia_services_ptexlib_ptlib($uid){
	 
 	$user = user_load($uid);
 	if(!$user){
        $data = array(
        	'success' => 0,
 		'message' => 'user: '.$uid.' not found'
        );		
 		return $data;
	
 	}
	
	
	global $user;
	 
 	$query = db_select('exercises', 'e');
 	$query->join('exercise_videos', 'ev', 'e.id = ev.exid');
 	$query->join('exercise_images', 'ei', 'e.id = ei.exid');
 	$query->fields('e');
 	// $query->addField('pe', 'id', 'pe_id');
 	$query->fields('ev');
 	$query->fields('ei');
 	// if($type == 'active'){
//  		$query->condition('pe.active', 1, '=');
//  	}
 	$query->condition('e.owner', $uid, '=');
	//$query->condition('e.pt_lib', 1, '=');
	$i = 0;
 	$result = $query->execute();
 	$exercises = array();
 	while ($record = $result->fetchAssoc()) {

	 	$query = db_select('exercise_categories', 'e');
	 	$query->join('exercise_x_category', 'ec', 'e.id = ec.ex_cat');
	 	$query->fields('ec');
	 	$query->fields('e');
		$query->condition('ec.ex_id', $record['exid'], '=');
		
	 	$excat = $query->execute();
		$categories = array();
		while($cat = $excat->fetchAssoc()){
			$categories[] = $cat['category_name'];
		}
	 	
		$record['categories'] = $categories;
		
		if(isset($record['pt_lib'])){
			if($record['pt_lib'] == 1) {
			 $record['webex'] = 0;	
			}else{
			 $record['webex'] = 1;
			}
		}else{
			$record['webex'] = 1;
		}
		
		
		
 		$exercises[] = $record;
		$i++;
 	}
	

    $data = array(
    	'success' => 1,
		'count' => $i,
	    'message' => 'found '.$i.' exercises for user: '.$uid,
    	'exercises' => $exercises
    );		
	return $data;
	
	
	
 }
 
 
 
 function geia_services_ptexlib_ptlib_create($uid,$data){
 	$user = user_load($uid);
 	if(!$user){
        $data = array(
        	'success' => 0,
 		'message' => 'user: '.$uid.' not found'
        );		
 		return $data;
	
 	}
	
     $profile = profile2_load_by_user($user,'main');
     $data['profile'] = $profile;
     $data['account'] = $user;

     $name = $data['video_title'];
	 
     $video_name = $data['video_name'];
     $video_name = str_replace(' ', '',preg_replace("/[^A-Za-z0-9 ]/", '', $name)).'-'.$uid.'-'.$video_name;
     $video_name = str_replace(' ', '',$video_name);
     $videoData = base64_decode($data['video_data']);
     $video = file_save_data($videoData, 'public://'.$video_name, FILE_EXISTS_REPLACE);

     $video_image_name = $data['video_image_name'];
     $video_image_name = str_replace(' ', '',preg_replace("/[^A-Za-z0-9 ]/", '', $name)).'-'.$uid.'-'.$video_image_name.'.jpg';
     $videoimageData = base64_decode($data['video_image']);
     $image = file_save_data($videoimageData, 'public://'.$video_image_name, FILE_EXISTS_REPLACE);

     $video_url = file_create_url($video->uri);
     $image_url = image_style_url('video_image', $image->uri);


     $record['owner'] = $uid;
     $record['webexcode'] = $video_name;
     $record['title'] = $name;
     $record['created'] = time();
     $record['updated'] = time();
	 $record['pt_lib'] = 1;
	 $record['active'] = 1;
	 $record['notes'] = $data['notes'];
	 $record['comments'] = $data['comments'];

     $recordv['mp4'] = $video_url;
     $recordv['created'] = time();
     $recordv['updated'] = time();

     $recordi['image1'] = $image_url;
     $recordi['created'] = time();
     $recordi['updated'] = time();
	 
	 
     $id = 0;

     $query = db_select('exercises', 'e');
     $query->fields('e');
     $query->condition('e.owner',$uid,'=');
     $query->condition('e.webexcode',$video_name,'=');
     $result = $query->execute();

     while ($rdata = $result->fetchAssoc()) {
     	$id = $rdata['id'];
     }
	 
	 
	 
	
	 
     if ($id == 0) {
         $record['created'] = time();
         $record['updated'] = time();
         	$id = db_insert('exercises')
         	      ->fields($record)
         	      ->execute();
         watchdog('exercises', "ID  = " . $id);

         //add video
         $recordv['exid'] = $id;
         db_insert('exercise_videos')
             ->fields($recordv)
             ->execute();
         watchdog('exercise_video', "ID  = " . $id);

         //add image

         $recordi['exid'] = $id;
         db_insert('exercise_images')
             ->fields($recordi)
             ->execute();
         watchdog('exercise_image', "ID  = " . $id);
		 
		 
	 	if(isset($data['categories'])){
			
	 	  foreach($data['categories'] as $category){
             _exercise_category($category,$id);
	 	  }

	 	}



     } else {
		 
		 
		 
         $record['updated'] = time();
         db_update('exercises')
             ->fields($record)
             ->condition('id',$id,'=')
             ->execute();

         //videos
         db_update('exercise_videos')
             ->fields($recordv)
             ->condition('exid',$id,'=')
             ->execute();

         //images
         db_update('exercise_images')
             ->fields($recordi)
             ->condition('exid',$id,'=')
             ->execute();
		 
		 
 	 	if(isset($data['categories'])){

 	 	  foreach($data['categories'] as $category){
               _exercise_category($category,$id);
 	 	  }

 	 	}
	 }

     return array(
         'success' => 1,
         'message' => 'Custom Exercise Data Saved',
     );

     
 }
 
 
 function geia_services_ptexlib_ptlib_update($uid,$data){
 	$user = user_load($uid);
 	if(!$user){
        $data = array(
        	'success' => 0,
 		'message' => 'user: '.$uid.' not found'
        );		
 		return $data;
	
 	}
	

     $profile = profile2_load_by_user($user,'main');
     $data['profile'] = $profile;
     $data['account'] = $user;
     
	 if(isset($data['exid'])){
	 	 $exid = $data['exid'];
	 }
	
     $name = $data['video_title'];
	 
	 if(isset($data['video_data'])){
		 
	     $video_name = $data['video_name'];
		 $record['webexcode'] = $video_name;
	     $video_name = str_replace(' ', '',preg_replace("/[^A-Za-z0-9 ]/", '', $name)).'-'.$uid.'-'.$video_name;
	     $video_name = str_replace(' ', '',$video_name);
	     $videoData = base64_decode($data['video_data']);
	     $video = file_save_data($videoData, 'public://'.$video_name, FILE_EXISTS_REPLACE);

	     $video_image_name = $data['video_image_name'];
	     $video_image_name = str_replace(' ', '',preg_replace("/[^A-Za-z0-9 ]/", '', $name)).'-'.$uid.'-'.$video_image_name.'.jpg';
	     $videoimageData = base64_decode($data['video_image']);
	     $image = file_save_data($videoimageData, 'public://'.$video_image_name, FILE_EXISTS_REPLACE);

	     $video_url = file_create_url($video->uri);
	     $image_url = image_style_url('video_image', $image->uri);
		 
	     $recordv['mp4'] = $video_url;
	     $recordv['created'] = time();
	     $recordv['updated'] = time();

	     $recordi['image1'] = $image_url;
	     $recordi['created'] = time();
	     $recordi['updated'] = time();
	 	
	 }
     


     $record['owner'] = $uid;
     
     $record['title'] = $name;
     $record['created'] = time();
     $record['updated'] = time();
	 $record['pt_lib'] = 1;
	 $record['notes'] = $data['notes'];
	 $record['comments'] = $data['comments'];

    
	 
	 
     $id = 0;

     $query = db_select('exercises', 'e');
     $query->fields('e');
     $query->condition('e.owner',$uid,'=');
     $query->condition('e.id',$exid,'=');
     $result = $query->execute();

     while ($rdata = $result->fetchAssoc()) {
     	$id = $rdata['id'];
     }
	 
	 
	 
	
	 
     if ($id == 0) {
         
	     return array(
	         'success' => 0,
	         'message' => 'Could not update Video, exid: '.$exid.' does not exist in the database',
	     );


     } else {
		 
		 
		 
         $record['updated'] = time();
         db_update('exercises')
             ->fields($record)
             ->condition('id',$id,'=')
             ->execute();
         if(isset($data['video_data'])){  //only update if video file was sent
         //videos
         	db_update('exercise_videos')
             	->fields($recordv)
                ->condition('exid',$id,'=')
             	->execute();

         //images
         db_update('exercise_images')
             	->fields($recordi)
             	->condition('exid',$id,'=')
             	->execute();
	     }
		 
		 
 	 	if(isset($data['categories'])){
               _exercise_delete_category($exid);
 	 	  foreach($data['categories'] as $category){
               _exercise_category($category,$id);
 	 	  }

 	 	}
	 }

     return array(
         'success' => 1,
         'message' => 'Custom Exercise Data Updated',
     );

     
 }
 
 function geia_services_ptexlib_ptlib_delete($uid,$exid){
	 
     $record['updated'] = time();
	 $record['active'] = 0;
    $result = db_update('exercises')
         ->fields($record)
         ->condition('id',$exid,'=')
		 ->condition('owner',$uid,'=')
         ->execute();
	
	if($result){
        return array(
            'success' => 1,
            'message' => 'Custom Exercise was removed from the library',
        );
	}else{
        return array(
            'success' => 0,
            'message' => 'Error: Custom Exercise was not removed from the library',
        );
	}
     
	
 }
 

 function _exercise_delete_category($exid){
      
	$num_deleted = db_delete('exercise_x_category')
	   ->condition('ex_id',$exid,'=')
	   ->execute();
	  
	return;
 }



 function _exercise_category($category,$exid){


     $query = db_select('exercise_categories', 'e');
     $query->fields('e');
     $query->condition('e.category_name',$category,'=');
     $result = $query->execute();
     $id = 0;
     while ($data = $result->fetchAssoc()) {
     	$id = $data['id'];
     }
 
  if($id == 0){
 	$record['category_name'] = $category;
      	$id = db_insert('exercise_categories')
      	      ->fields($record)
      	      ->execute();
        watchdog('created new ex category', "ID  = " . $id);
  }else{
	 
	 
      $query = db_select('exercise_x_category', 'ex');
      $query->fields('ex');
      $query->condition('ex.ex_id',$exid,'=');
 	  $query->condition('ex.ex_cat',$id,'=');
      $result = $query->execute();
      $cid = 0;
      while ($data = $result->fetchAssoc()) {
      	$cid = $data['id'];
      }
	 
	 
 	 	if($cid == 0){
   			$record['ex_cat'] = $id;
  			$record['ex_id'] = $exid;
         	$id = db_insert('exercise_x_category')
         	      ->fields($record)
         	      ->execute();
           	watchdog('set exercise category', "ID  = " . $id);

  	 	}else{
 		 	return;
  	 	}
	}
	
	 return;
 }
		

