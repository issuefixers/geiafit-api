<?php /*require_once("includes/braintree_init.php"); */?>

<html>
<?php require_once("includes/head.php"); ?>



<body>

<style>
    .hosted-field {
        height: 50px !important;
    }
    input {
        font-size: 18px;
    }
</style>


<?php require_once("includes/header.php"); ?>

<div class="wrapper">
    <div class="checkout container">

        <header style="margin-top: 30px;">
            <h1>Hello, <span class="client_name"></span></h1>

        </header>

        <h2 class="message">...</h2>
        <h4 class="message-error" style="color: red;"></h4>
        <h4 class="message-ok" style="color: green;"></h4>
        <h4 class="message-info" style="color: navy;"></h4>


        <form id="payment-form" style="display: none;">
            <section>

                <div class="payment-info" style="">
                    <div id="error-message"></div>

                    <label for="card-number">Card Number</label>
                    <div class="hosted-field" id="card-number"></div>

                    <label for="cvv">CVV</label>
                    <div class="hosted-field" id="cvv"></div>

                    <label for="expiration-date">Expiration Date</label>
                    <div class="hosted-field" id="expiration-date"></div>
                </div>

            </section>

            <div style="text-align: right;">
                <button class="button subscribe"><span>SUBSCRIBE</span></button>
            </div>

        </form>
    </div>
</div>

<!-- Load the Client component. -->
<script src="https://js.braintreegateway.com/web/3.1.0/js/client.min.js"></script>

<!-- Load the Hosted Fields component. -->
<script src="https://js.braintreegateway.com/web/3.1.0/js/hosted-fields.min.js"></script>


<script type="text/javascript">

    var msg = function (m) {
        $('.message').html(m);
    }

    var error = function (m) {
        $('.message-error').html(m);
    }

    var ok = function (m) {
        $('.message-ok').html(m);
    }

    var info = function (m) {
        $('.message-info').html(m);
    }

    var hide = function () {
        msg('');
        error('');
        ok('');
        info('');
    }

    $(document).ready(function() {
        msg("Checking subscription status. Pl's wait a little bit ...");
        $.get("/api/bt/check", function(data) {
            console.log(data);
            hide();
            if (data.success) {
              $('.client_name').html(data.client_name);
              if (data.allowed) {
                  if (typeof(data.message) == 'string') {
                    ok(data.message);
                  }
              } else {
                if (typeof(data.token) == 'string') {
                    if (typeof(data.message) == 'string') {
                      info(data.message);
                    }

                    braintree.client.create({
                        authorization: data.token
                    }, function (clientErr, clientInstance) {
                        if (clientErr) {
                            // Handle error in client creation
                            error('Error in Braintree client creation');
                            return;
                        }
                        braintree.hostedFields.create({
                            client: clientInstance,
                            styles: {
                                'input': {
                                    'font-size': '18px'
                                },
                                'input.invalid': {
                                    'color': 'red'
                                },
                                'input.valid': {
                                    'color': 'green'
                                }
                            },
                            fields: {
                                number: {
                                    selector: '#card-number',
                                    placeholder: '4111 1111 1111 1111'
                                },
                                cvv: {
                                    selector: '#cvv',
                                    placeholder: '123'
                                },
                                expirationDate: {
                                    selector: '#expiration-date',
                                    placeholder: '10 / 2019'
                                }
                            }
                        }, function (hostedFieldsErr, hostedFieldsInstance) {
                            if (hostedFieldsErr) {
                                // Handle error in Hosted Fields creation
                                return;
                            }
                            $('#payment-form').show();

                            $('.subscribe').on('click', function (e) {
                                e.preventDefault();
                                if (confirm('Are you sure?')) {
                                    hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                                        if (tokenizeErr) {
                                            // Handle error in Hosted Fields tokenization
                                            error(tokenizeErr.message);
                                            return;
                                        }
                                        $('#payment-form').hide();
                                        hide();
                                        msg("Pl's wait a little bit ...");
                                        //console.log(payload);
                                        $.get("/api/bt/subscribe/" + payload.nonce, function (data) {
                                           hide();
                                           console.log(data);
                                           if (data.success) {
                                             location.reload();
                                           } else {
                                               if (typeof(data.message) == 'string') {
                                                   error(data.message);
                                               } else {
                                                   error('Unknown error');
                                               }
                                           }
                                        });
                                    });
                                }
                            });
                        });
                    });
                }
              }
            } else {
                if (typeof(data.message) == 'string') {
                  error(data.message);
                } else {
                  error('Unknown error');
                }
            }
        });



    });
</script>
</body>
</html>
